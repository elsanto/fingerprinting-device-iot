import os
import errno

"""
load the logging configuration
"""
import logging
from logging.config import fileConfig

fileConfig('logging.ini')
logger = logging.getLogger()


class WriteMode:
    APPEND_MODE = 'a'
    REPLACE_MODE = 'w'


class DataWriter:
    """
    This class writes the collected features to an output file in the correct
    directory, according to the data provided by the user.

    DAT files are organized using the following directory structure:

    data/dataset/(PCP FOLDER DIRECTORY)/(PCP FOLDER NAME).txt
    """

    def __init__(self, pcap_file_path, mode=WriteMode.REPLACE_MODE):

        self._pcap_file_path = pcap_file_path
        self._file_path = self._get_file_path()

        self._f = open(self._file_path, mode)

    def _get_file_path(self):
        path = None

        # get parent directory
        filename = DataWriter.get_file_with_parents(self._pcap_file_path)
        # remove pcp extension
        filename = os.path.splitext(filename)[0]
        path = "data/dataset/"
        path = "%s%s.txt" % (path, filename)

        DataWriter._recursively_create_dirs(path)

        logger.info("Writing data to: %s" % path)

        return path

    @staticmethod
    def get_file_with_parents(filepath, levels=1):
        common = filepath
        for i in range(levels + 1):
            common = os.path.dirname(common)
        return os.path.relpath(filepath, common)

    @staticmethod
    def _recursively_create_dirs(file_path):
        """
        Creates the directories if they do not exist
        """
        directory = os.path.dirname(file_path)
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

    @staticmethod
    def _get_txt_files_in_folder(_folder=None):
        if _folder is None:
            _folder = os.path.abspath('./data/dataset')
        file_paths = []

        file_name_ending = ".txt"

        for root, subdirs, files in os.walk(_folder):
            for f in files:
                if f.endswith(file_name_ending):
                    file_paths.append(os.path.join(root, f))

        return file_paths

    @staticmethod
    def _get_dic_files_in_folder(path, excludes=[]):
        pcapFiles = {}

        for root, dirs, files in os.walk(path, topdown=True):

            dirs[:] = [d for d in dirs if d not in excludes and not d.startswith('.')]
            parent_dir = root.split(os.path.sep)[-1]

            #creating dict keys
            for dir in dirs:
                pcapFiles[dir] = []

            # adding pcap as items
            for file in files:
                if file.endswith(".pcap"):

                    pcapFiles[parent_dir].append(os.path.join(root, file))

        return pcapFiles


    @staticmethod
    def _multiple_files_to_one_file(_files,_outfile=None):
        if _outfile is None:
            _outfile = os.path.abspath('./data/model/train.txt')

        with open(_outfile, "wb") as outfile:
            for f in _files:
                with open(f, "rb") as infile:
                    outfile.write(infile.read())

    def close_file(self):
        self._f.close()

    def append_feature(self, feature):
        #print(feature)
        self._f.write("%s\n" % feature)


if __name__ == "__main__":

    #write features to model/train.txt
    fs=DataWriter._get_txt_files_in_folder()
    #output_iot_analytics='/Users/cmaniraguha/iot-device-fingerprinting/data/model/train_iot_analytics_17juin_src_host.txt'
    #output_iot_sentinel='/Users/cmaniraguha/iot-device-fingerprinting/data/model/train_iot_sentinel_16mai_sampled_1hour.txt'
    output_iot_labo='/Users/cmaniraguha/iot-device-fingerprinting/data/model/train_iot_labo_07_aout.txt'
    DataWriter._multiple_files_to_one_file(fs,output_iot_labo)