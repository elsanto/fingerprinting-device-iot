#!/usr/bin/python

from scapy.all import *
import numpy as np
import config
from dataWriter import DataWriter
import socket
"""
load the logging configuration
"""
import logging
from logging.config import fileConfig

fileConfig('logging.ini')
logger = logging.getLogger()


class PcpFilter:
    def __init__(self, prot=None, src_ip=None, dst_ip=None, exclude_private_communication=False):
        self.protocol_label = prot
        self.srcIP = src_ip
        self.dstIP = dst_ip
        self.exclude_private_ips=exclude_private_communication

    def filter(self, pkt):

        #no ipV6 support for the moment
        if not pkt.haslayer('IP'):
            return False


        if self.exclude_private_ips:

            if filter.isIpPrivate(pkt["IP"].src) and filter.isIpPrivate(pkt["IP"].dst):
                #print("private : "+pkt["IP"].src +" > "+pkt["IP"].dst)
                return False

        if self.protocol_label is not None:

            b_prot = False
            for protocol in self.protocol_label:
                if pkt.haslayer(protocol):
                    b_prot = True

            if not b_prot:
                return False

        if self.srcIP is not None or self.dstIP is not None:

            if not pkt.haslayer("IP"):
                return False

            if  self.srcIP is not None and pkt["IP"].src == self.srcIP:
                #print(pkt["IP"].src +'==>>>>'+ self.srcIP)

                return True

            if self.dstIP is not None and pkt["IP"].dst == self.dstIP:

                #print(pkt["IP"].dst +'<<<<=='+ self.dstIP)
                return True

            return False


        return True

    #https://stackoverflow.com/questions/691045/how-do-you-determine-if-an-ip-address-is-private-in-python
    def isIpPrivate(ip):
        """Use a standard ip like "192.43.21.1" """
        f = struct.unpack('!I',socket.inet_pton(socket.AF_INET,ip))[0]
        private = (
            [ 2130706432, 4278190080 ], # 127.0.0.0,   255.0.0.0   http://tools.ietf.org/html/rfc3330
            [ 3232235520, 4294901760 ], # 192.168.0.0, 255.255.0.0 http://tools.ietf.org/html/rfc1918
            [ 2886729728, 4293918720 ], # 172.16.0.0,  255.240.0.0 http://tools.ietf.org/html/rfc1918
            [ 167772160,  4278190080 ], # 10.0.0.0,    255.0.0.0   http://tools.ietf.org/html/rfc1918
        )
        for net in private:
            if (f & net[1] == net[0]):
                return True
        return False

class Flow:

    # TCP control flags
    TH_FIN = 0x01  # end of data
    TH_SYN = 0x02  # synchronize sequence numbers
    TH_RST = 0x04  # reset connection
    TH_PUSH = 0x08  # push
    TH_ACK = 0x10  # acknowledgment number set
    TH_URG = 0x20  # urgent pointer set
    TH_ECE = 0x40  # ECN echo, RFC 3168
    TH_CWR = 0x80  # congestion window reduced

    '''
    https://null-byte.wonderhowto.com/how-to/hack-like-pro-conduct-passive-os-fingerprinting-with-p0f-0151191/
    https://github.com/secdev/scapy/blob/master/scapy/modules/p0f.py#L3
    Fourth: The Windows Field
    
    The Window field sets the size of the buffer of the sending system. 
    This is the way that TCP maintains flow control. This field alone varies the most among operating systems. 
    If you can find this value, you have about 70-80% chance of determining the operating system that sent the packet.
    '''

    #"cwr_flag", "ece_flag","mdns" removed
    FEATURES = config.FEATURES

    def __init__(self, pkt, initFeatures):

        self.featureList = initFeatures
        self.src = pkt['IP'].src
        self.dst = pkt['IP'].dst
        try:
            self.protocol_label = config.PROTOCOLS_NUMBER[pkt['IP'].proto]
            self.protocole_number = pkt['IP'].proto
        except KeyError:
            logger.error("IP Protocol number not recognized : %s " % pkt['IP'].proto)
            exit(1)

        #push flag
        self.push_flag=0

        #ack_flag
        '''
        only acknowledging data with payload==0
        keepalive packet
        '''
        self.ack_flag_nodata=0

        #cwr_flag
        self.cwr_flag=0

        #ece_flag
        self.ece_flag=0

        self.__ports = []

        self.__time = []
        self.__pktLen = []
        self.__interArrivalTime = []
        self.__windowSize = []

        #SSDP attribute
        self.ssdp = 0

        #HTTPS attribute
        self.https = 0

        #HTTP attribute
        self.http = 0

        #mdns attribute
        self.mdns = 0

        #ntp attribute
        self.ntp = 0

        #bootp attribute
        self.bootp = 0

        #dns attribute
        self.dns = 0

        #_______ DNS stats_____

        # Dns answers ttl
        self.__dns_response_ttl = []

        # Answers resource records
        self.__dns_response_answers_rrs = []

        # Additional resource records
        self.__dns_response_additional_rrs = []

        # Authoritative resource records
        self.__dns_response_authority_rrs = []

        # TYPE A (IPv4)
        self.__dns_response_type_a = []

        # TYPE CANONICAL NAME (CNAME)
        self.__dns_response_type_cname = []

        # TYPE AAAA (IPv6)
        self.__dns_response_type_aaaa = []

        #private ip type
        self.is_private=0





    #@property

    def get_pktLen(self):
        return self.__pktLen
    def get_pktLen_src(self):
        return self.__pktLen_src
    def get_pktLen_dst(self):
        return self.__pktLen_dst

    def set_pktLen(self, pktLen):
        self.__pktLen = pktLen
    def set_pktLen_src(self, pktLen_src):
        self.__pktLen_src = pktLen_src
    def set_pktLen_dst(self, pktLen_dst):
        self.__pktLen_dst = pktLen_dst


    def get_window_size(self):
        return self.__windowSize


    def set_window_size(self, win_s):
        self.__windowSize = win_s

    def get_time(self):
        return self.__time


    def set_time(self, time):
        self.__time = time



    def get_dns_response_ttl(self):
        return self.__dns_response_ttl

    def set_dns_response_ttl(self, dns_response_ttl):
        self.__dns_response_ttl = dns_response_ttl


    def get_dns_response_answers_rrs(self):
        return self.__dns_response_answers_rrs

    def set_dns_response_answers_rrs(self, dns_response_answers_rrs):
        self.__dns_response_answers_rrs = dns_response_answers_rrs


    def get_dns_response_additional_rrs(self):
        return self.__dns_response_additional_rrs

    def set_dns_response_additional_rrs(self, dns_response_additional_rrs):
        self.__dns_response_additional_rrs = dns_response_additional_rrs



    def get_dns_response_authority_rrs(self):
        return self.__dns_response_authority_rrs

    def set_dns_response_authority_rrs(self, dns_response_authority_rrs):
        self.__dns_response_authority_rrs = dns_response_authority_rrs



    def get_dns_response_type_a(self):
        return self.__dns_response_type_a

    def set_dns_response_type_a(self, dns_response_type_a):
        self.__dns_response_type_a = dns_response_type_a



    def get_dns_response_type_cname(self):
        return self.__dns_response_type_cname

    def set_dns_response_type_cname(self, dns_response_type_cname):
        self.__dns_response_type_cname = dns_response_type_cname


    def get_dns_response_type_aaaa(self):
        return self.__dns_response_type_aaaa

    def set_dns_response_type_aaaa(self, dns_response_type_aaaa):
        self.__dns_response_type_aaaa = dns_response_type_aaaa


    def get_ports(self):
        return self.__ports

    def set_ports(self, port):
        self.__ports = port



    def tcp_ports(self):
        return len(self.__ports)

    #delay,interpacket arrival time
    def inter_arrival_time(self):

        """
        flows containing only a single packet would
provide no inter-arrival time statistics and packet
length statistics can only be computed in the forward
direction.
        :return:
        """
        if len(self.__time) <= 1:
            self.__interArrivalTime.append(0)
            return

        self.__time.sort()
        #https://sciencing.com/calculate-interarrival-time-5417319.html
        for i in range(1, len(self.__time)):
            interval = self.__time[i] - self.__time[i - 1]
            self.__interArrivalTime.append(abs(interval))

     # flow Duration in seconds precision .2
    def flow_duration(self):
        return sum(self.__interArrivalTime) if len(self.__time) > 0 else 0


    def avg_bits_sec(self):
        return  (self.total_bytes() * 8 / sum(self.__interArrivalTime)) if self.flow_duration() > 0 else 0

    def avg_window_size(self):
        return  np.mean(self.__windowSize)

    def mean_inter_arr_time(self):
        return np.mean(self.__interArrivalTime)

    def var_inter_arr_time(self):
        return np.var(self.__interArrivalTime)

    def std_inter_arr_time(self):
        return np.std(self.__interArrivalTime)

    def min_inter_arr_time(self):
        return min(self.__interArrivalTime)

    def max_inter_arr_time(self):
        return max(self.__interArrivalTime)


    #------------- DNS statistics---------
    def mean_dns_response_ttl(self):
        return np.mean(self.__dns_response_ttl)if len(self.__dns_response_ttl) > 0 else 0

    def var_dns_response_ttl(self):
        return np.var(self.__dns_response_ttl)if len(self.__dns_response_ttl) > 0 else 0

    def std_dns_response_ttl(self):
        return np.std(self.__dns_response_ttl)if len(self.__dns_response_ttl) > 0 else 0

    def min_dns_response_ttl(self):
        return min(self.__dns_response_ttl)if len(self.__dns_response_ttl) > 0 else 0

    def max_dns_response_ttl(self):
        return max(self.__dns_response_ttl)if len(self.__dns_response_ttl) > 0 else 0


    def mean_dns_response_answers_rrs(self):
        return np.mean(self.__dns_response_answers_rrs)if len(self.__dns_response_answers_rrs) > 0 else 0

    def var_dns_response_answers_rrs(self):
        return np.var(self.__dns_response_answers_rrs)if len(self.__dns_response_answers_rrs) > 0 else 0

    def std_dns_response_answers_rrs(self):
        return np.std(self.__dns_response_answers_rrs)if len(self.__dns_response_answers_rrs) > 0 else 0

    def min_dns_response_answers_rrs(self):
        return min(self.__dns_response_answers_rrs)if len(self.__dns_response_answers_rrs) > 0 else 0

    def max_dns_response_answers_rrs(self):
        return max(self.__dns_response_answers_rrs)if len(self.__dns_response_answers_rrs) > 0 else 0



    def mean_dns_response_additional_rrs(self):
        return np.mean(self.__dns_response_additional_rrs)if len(self.__dns_response_additional_rrs) > 0 else 0

    def var_dns_response_additional_rrs(self):
        return np.var(self.__dns_response_additional_rrs)if len(self.__dns_response_additional_rrs) > 0 else 0

    def std_dns_response_additional_rrs(self):
        return np.std(self.__dns_response_additional_rrs)if len(self.__dns_response_additional_rrs) > 0 else 0

    def min_dns_response_additional_rrs(self):
        return min(self.__dns_response_additional_rrs)if len(self.__dns_response_additional_rrs) > 0 else 0

    def max_dns_response_additional_rrs(self):
        return max(self.__dns_response_additional_rrs)if len(self.__dns_response_additional_rrs) > 0 else 0


    def mean_dns_response_authority_rrs(self):
        return np.mean(self.__dns_response_authority_rrs)if len(self.__dns_response_authority_rrs) > 0 else 0

    def var_dns_response_authority_rrs(self):
        return np.var(self.__dns_response_authority_rrs)if len(self.__dns_response_authority_rrs) > 0 else 0

    def std_dns_response_authority_rrs(self):
        return np.std(self.__dns_response_authority_rrs)if len(self.__dns_response_authority_rrs) > 0 else 0

    def min_dns_response_authority_rrs(self):
        return min(self.__dns_response_authority_rrs)if len(self.__dns_response_authority_rrs) > 0 else 0

    def max_dns_response_authority_rrs(self):
        return max(self.__dns_response_authority_rrs)if len(self.__dns_response_authority_rrs) > 0 else 0


    def mean_dns_response_type_a(self):
        return np.mean(self.__dns_response_type_a)if len(self.__dns_response_type_a) > 0 else 0

    def var_dns_response_type_a(self):
        return np.var(self.__dns_response_type_a)if len(self.__dns_response_type_a) > 0 else 0

    def std_dns_response_type_a(self):
        return np.std(self.__dns_response_type_a)if len(self.__dns_response_type_a) > 0 else 0

    def min_dns_response_type_a(self):
        return min(self.__dns_response_type_a)if len(self.__dns_response_type_a) > 0 else 0

    def max_dns_response_type_a(self):
        return max(self.__dns_response_type_a)if len(self.__dns_response_type_a) > 0 else 0


    def mean_dns_response_type_cname(self):
        return np.mean(self.__dns_response_type_cname)if len(self.__dns_response_type_cname) > 0 else 0

    def var_dns_response_type_cname(self):
        return np.var(self.__dns_response_type_cname)if len(self.__dns_response_type_cname) > 0 else 0

    def std_dns_response_type_cname(self):
        return np.std(self.__dns_response_type_cname)if len(self.__dns_response_type_cname) > 0 else 0

    def min_dns_response_type_cname(self):
        return min(self.__dns_response_type_cname)if len(self.__dns_response_type_cname) > 0 else 0

    def max_dns_response_type_cname(self):
        return max(self.__dns_response_type_cname)if len(self.__dns_response_type_cname) > 0 else 0


    def mean_dns_response_type_aaaa(self):
        return np.mean(self.__dns_response_type_aaaa)if len(self.__dns_response_type_aaaa) > 0 else 0

    def var_dns_response_type_aaaa(self):
        return np.var(self.__dns_response_type_aaaa)if len(self.__dns_response_type_aaaa) > 0 else 0

    def std_dns_response_type_aaaa(self):
        return np.std(self.__dns_response_type_aaaa)if len(self.__dns_response_type_aaaa) > 0 else 0

    def min_dns_response_type_aaaa(self):
        return min(self.__dns_response_type_aaaa)if len(self.__dns_response_type_aaaa) > 0 else 0

    def max_dns_response_type_aaaa(self):
        return max(self.__dns_response_type_aaaa)if len(self.__dns_response_type_aaaa) > 0 else 0

    #-------END------ DNS statistics---------


    def avg_pkt_len(self):
        return np.mean(self.__pktLen) if len(self.__pktLen) > 0 else 0

    def var_pkt_len(self):
        return np.var(self.__pktLen)

    def std_pkt_len(self):
        return np.std(self.__pktLen)

    def min_bytes(self):
        return min(self.__pktLen)

    def max_bytes(self):
        return max(self.__pktLen)

    def total_pkts(self):
        return len(self.__pktLen)

    def total_bytes(self):
        return sum(self.__pktLen)

    def extract_packet(self, pkt):
        #push_flag
        if self.protocol_label == 'TCP':
            if pkt['TCP'].flags & self.TH_PUSH:
                self.push_flag += 1

            #A keepalive probe is a packet with no data in it and the ACK flag turned on

            if (pkt['TCP'].flags & self.TH_ACK) and int(len(pkt['TCP'].payload)) == 0:
                self.ack_flag_nodata += 1


            #cwr_flag

            if pkt['TCP'].flags & self.TH_CWR:
                self.cwr_flag += 1


            #ece_flag
            if pkt['TCP'].flags & self.TH_ECE:
                self.ece_flag += 1

        #ssdp
        if pkt[self.protocol_label].sport == 1900:
            self.ssdp += 1

        #https
        if pkt[self.protocol_label].sport == 443:
            self.https += 1

        #http
        if pkt[self.protocol_label].sport == 80 or pkt[self.protocol_label].dport == 80:
            self.http += 1

        #mdns
        if pkt[self.protocol_label].sport == 5353:
            self.mdns += 1

        #ntp
        if pkt[self.protocol_label].sport == 123 or pkt[self.protocol_label].dport == 123:
            self.ntp += 1

        #bootp
        if pkt[self.protocol_label].sport == 67 or pkt[self.protocol_label].sport == 68:
            self.bootp += 1

        #dns
        if pkt[self.protocol_label].sport == 53 or pkt[self.protocol_label].dport == 53:
            self.dns += 1
            #https://routley.io/tech/2017/12/28/hand-writing-dns-messages.html
            #http://www.firewall.cx/networking-topics/protocols/domain-name-system-dns/161-protocols-dns-response.html


            if pkt.haslayer("DNSRR"):#  and  pkt['UDP'].dport==45028:
                #looping in dns answers

                __cnt_type_a = 0
                __cnt_type_cname = 0
                __cnt_type_aaaa = 0
                for i in range(pkt.ancount):
                    self.__dns_response_ttl.append(pkt.an[i].ttl)

                    #type A
                    if pkt.an[i].type == 1:
                        __cnt_type_a += 1

                    #type CNAME
                    if pkt.an[i].type == 5:
                        __cnt_type_cname += 1

                    #type AAAA
                    if pkt.an[i].type == 28:
                        __cnt_type_aaaa += 1


                self.__dns_response_type_a .append( __cnt_type_a)
                self.__dns_response_type_cname.append(__cnt_type_cname)
                self.__dns_response_type_aaaa.append( __cnt_type_aaaa)

                #http://www.zytrax.com/books/dns/ch15/
                #https://blog.dnsimple.com/2015/03/whats-in-a-dns-response/

                #DNS answers count
                if pkt.ancount:
                    self.__dns_response_answers_rrs.append(pkt.ancount)

                #DNS authoritative nameservers count
                if pkt.nscount:
                    self.__dns_response_authority_rrs.append(pkt.nscount)

                #DNS additional records
                if pkt.arcount:
                    self.__dns_response_additional_rrs.append(pkt.arcount)



                #https://stackoverflow.com/questions/31980648/retrieve-dns-flags-information-in-hexadecimal-format
                '''
                flagg=bytes(pkt['DNS'])[2:4]
                ddns=pkt['DNS']
                qname=pkt['DNS'].qd.qname       # the original query name
                ip = pkt['IP']
                udp = pkt['UDP']
                print ("[*] response: %s:%s <- %s:%d : " % (
                    ip.dst, udp.dport,
                    ip.src, udp.sport))
                
                for i in range(pkt.ancount):
                    dnsrr = pkt.an[i]

                    print ("    Answers:  ttl = %d,  type = %d,    NAME = %s , CNAME = %s" % ( dnsrr.ttl , dnsrr.type , dnsrr.rrname , dnsrr.rdata))
                print('_____'*10)
                #https://itgeekchronicles.co.uk/2013/10/16/scapy-pcap-2-dns/
                for i in range(pkt.nscount):
                    dnsns = pkt.ns[i]

                    print ("    Authoritative nameservers:  ttl = %d,  type = %d,    NAME =  %s , CNAME =  %s" % (dnsns.ttl,dnsns.type,dnsns.rrname, dnsns.rdata))
                print('_____'*10)

                for i in range(pkt.arcount):
                    dnsar= pkt.ar[i]

                    print ("    Additional records:  ttl = %d,  type = %d,    NAME =  %s , CNAME =  %s" % (dnsar.ttl,dnsar.type,dnsar.rrname, dnsar.rdata))
                '''

        self.is_private = 1 if PcpFilter.isIpPrivate(self.src) and PcpFilter.isIpPrivate(self.dst) else 0

    def flowStats(self):
        self.inter_arrival_time()

        self.featureList.append(self.protocole_number)
        # Flow statistics
        self.featureList.extend([self.push_flag,
                                 self.ack_flag_nodata,

                                 self.flow_duration(),
                                 self.avg_bits_sec(),
                                 self.avg_window_size(),
                                 self.mean_inter_arr_time(),
                                 self.std_inter_arr_time(),
                                 self.min_inter_arr_time(),
                                 self.max_inter_arr_time(),
                                 self.avg_pkt_len(),
                                 self.std_pkt_len(),
                                 self.min_bytes(),
                                 self.max_bytes(),
                                 self.total_pkts(),
                                 self.total_bytes(),

                                 self.ssdp,
                                 self.https,
                                 self.http,

                                 self.ntp,
                                 self.bootp,
                                 self.dns,

                                 self.mean_dns_response_ttl(),
                                 self.std_dns_response_ttl(),
                                 self.min_dns_response_ttl(),
                                 self.max_dns_response_ttl(),

                                 self.mean_dns_response_answers_rrs(),
                                 self.std_dns_response_answers_rrs(),
                                 self.min_dns_response_answers_rrs(),
                                 self.max_dns_response_answers_rrs(),

                                 self.mean_dns_response_additional_rrs(),
                                 self.std_dns_response_additional_rrs(),
                                 self.min_dns_response_additional_rrs(),
                                 self.max_dns_response_additional_rrs(),

                                 self.mean_dns_response_authority_rrs(),
                                 self.std_dns_response_authority_rrs(),
                                 self.min_dns_response_authority_rrs(),
                                 self.max_dns_response_authority_rrs(),

                                 self.mean_dns_response_type_a(),
                                 self.std_dns_response_type_a(),
                                 self.min_dns_response_type_a(),
                                 self.max_dns_response_type_a(),

                                 self.mean_dns_response_type_cname(),
                                 self.std_dns_response_type_cname(),
                                 self.min_dns_response_type_cname(),
                                 self.max_dns_response_type_cname(),

                               # self.is_private,


                                 ],)



        return self.featureList




class FeatureExtractor:
    """
    CMA HOW TO COMMENT
    """

    def __init__(self, pcap_file_path, label, _filters):
        self.featureList = []
        self.flows = []
        self.sessionTime = None
        self.sessionSamplingPeriod = config.SAMPLING_TS

        self.pcapFile = pcap_file_path
        self.label = label

        self.filter = _filters


        self.flow_list= {}



    def extract_pkt(self, _packet):




       # _packet.show()


        # Process packets which are matched by input filter
        if self.filter.filter(_packet):
            if self.sessionTime == None:
                self.sessionTime = _packet.time + self.sessionSamplingPeriod


            if (_packet.time > self.sessionTime):
                self.sessionTime = self.sessionTime + self.sessionSamplingPeriod

            # A flow is defined by traffic that has the same source IP, destination IP, protocol, source port, and destination port
            key='{stime} {srcip} {dstip} {srcport} {dstport}'.format(stime=self.sessionTime,srcip=_packet['IP'].src, dstip=_packet['IP'].dst, srcport=_packet['IP'].sport, dstport=_packet['IP'].dport)

            if (key in self.flow_list) :                                                 # check if the packet belongs to already existing flow
                the_flow =self.flow_list.get(key)

                fpktLen=the_flow.get_pktLen()
                fpktLen.append(len(_packet))
                the_flow.set_pktLen(fpktLen)

                ftime=the_flow.get_time()
                ftime.append(_packet.time)
                the_flow.set_time(ftime)

                #windows size
                fwin=the_flow.get_window_size()
                win = getattr(_packet.payload, 'window', 0)
                fwin.append(int(win))
                the_flow.set_window_size(fwin)

                #ports
                fport=the_flow.get_ports()
                if _packet['IP'].sport not in fport:
                    fport.append(_packet['IP'].sport)
                    the_flow.set_ports(fport)




                the_flow.extract_packet(_packet)
                self.flow_list[key]=the_flow


            else:                                                                   # if this packet is the first one in this NEW flow

                init_features = [self.label]

                new_flow = Flow(_packet, init_features)
                new_flow.set_pktLen([len(_packet)])
                new_flow.set_time([_packet.time])

                win = getattr(_packet.payload, 'window', 0)
                new_flow.set_window_size([int(win)])


                #ports
                aport=new_flow.get_ports()

                if _packet['IP'].sport not in aport:
                    aport.append(_packet['IP'].sport)
                    new_flow.set_ports(aport)

                new_flow.extract_packet(_packet)


                self.flow_list[key]=new_flow                                         # Add new flow to the flow list


    def extract_features(self):
        """
         REad the pcap file to read packets from

         """

        # Feature extraction from pcap
        try:
            with PcapReader(self.pcapFile) as pcap:
                for packet in pcap:
                    self.extract_pkt(packet)
        except IOError:
            logger.error("Failed pcp file : %s " % self.pcapFile)

        # Append all features
        for key, aflow in self.flow_list.items():

            _attr=aflow.flowStats()

            # debug purpose insert ip address and port
            #_attr.insert(0,key)
            self.featureList.append(_attr)

#@todo:
#add feature retransmission, most queries http?


if __name__ == "__main__":

    #only tcp udp cousche transport (sport et dsport)
    filters = PcpFilter(['TCP', 'UDP'], None, None, False)


    #sentinel
    #pcapfiles=DataWriter._get_dic_files_in_folder('/Users/cmaniraguha/iot-device-fingerprinting/data/pcp/sentinel', [''])

    #iotanalytics


    #pcapfiles=DataWriter._get_dic_files_in_folder('/Users/cmaniraguha/iot-device-fingerprinting/data/pcp/iotanalytics_srchost', ['__ORIGINAL_PCAP'])

    #pcapfiles = {}
    #pcapfiles["hue"] = \
    #    ['/Users/cmaniraguha/iot-device-fingerprinting/data/pcp/cma/Bridge Hue/Bh10minPlayingOnOffMode192.168.2.4_18mars.pcap']

    #CMA


    pcapfiles=DataWriter._get_dic_files_in_folder('/Users/cmaniraguha/iot-device-fingerprinting/data/pcp/cma/LABO_04_08_2019', )

    # Prints the nicely formatted dictionary
    #import pprint
    #pprint.pprint(pcapfiles)



    #pcapfiles=DataWriter._get_dic_files_in_folder('/Users/cmaniraguha/Desktop/dns', ['BelkinWemoMotionSensor','Bridge Hue'])


    # Iterating the elements in list
    for label, files in pcapfiles.items():
        print(label)

        device_index = config.DEVICES_LABO.index(label)
        for pcapf in files:
            '''
            features extractions
            '''
            ft = FeatureExtractor(pcapf, device_index,  filters)
            ft.extract_features()
            '''
            writing to txt
            '''
            if len(ft.featureList):
                writer = DataWriter(pcapf)
                for feature in ft.featureList:
                    feat = ",".join(map(str, feature))
                    writer.append_feature(feat)
                writer.close_file()
                logger.debug("%d features extracted from %s \n" % (len(ft.featureList), pcapf))

    #write features to model/train.txt
    '''
    fs=DataWriter._get_txt_files_in_folder()

    DataWriter._multiple_files_to_one_file(fs)
    '''

'''
find . -name '*.txt' | xargs wc -l

'''
'''
tshark -r /Users/cmaniraguha/iot-device-fingerprinting/data/pcp/Bridge\ Hue/Bh10minPlayingOnOffMode192.168.2.4_18mars.pcap -T fields -e frame.time_relative -e ip.src -e ip.dst -e tcp.srcport -e tcp.dstport -e ip.len -e tcp.flags.push -e tcp.window_size -E separator='|' > /Users/cmaniraguha/iot-device-fingerprinting/data/pcp/Bridge\ Hue/Bh10minPlayingOnOffMode192.168.2.4_18mars.txt
'''