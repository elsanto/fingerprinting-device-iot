PROTOCOLS_NUMBER = {1: 'ICMP', 58: 'ICMPv6', 17: 'UDP',  6: 'TCP'}

SAMPLING_TS = 3600

FEATURES = ["label", "prot", "push_flag", "ack_flag_nodata",
            "flow_duration", "avg_bits_per_sec", "avg_window",
            "avg_arr_time", "std_arr_time", "min_arr_time", "max_arr_time",
            "avg_pkt_len",  "std_pkt_len",  "min_bytes", "max_bytes", "tot_pkt", "tot_bytes",
            "ssdp", "https","http", "ntp", "bootp", "dns",

            "avg_dns_response_ttl", "std_dns_response_ttl", "min_dns_response_ttl", "max_dns_response_ttl",
            "avg_dns_response_answers_rrs", "std_dns_response_answers_rrs", "min_dns_response_answers_rrs", "max_dns_response_answers_rrs",
            "avg_dns_response_additional_rrs", "std_dns_response_additional_rrs", "min_dns_response_additional_rrs", "max_dns_response_additional_rrs",
            "avg_dns_response_authority_rrs", "std_dns_response_authority_rrs", "min_dns_response_authority_rrs", "max_dns_response_authority_rrs",
            "avg_dns_response_type_a", "std_dns_response_type_a", "min_dns_response_type_a", "max_dns_response_type_a",
            "avg_dns_response_type_cname", "std_dns_response_type_cname", "min_dns_response_type_cname", "max_dns_response_type_cname",
    #        "is_private_ip"

            ]

DEVICES = ['Chacon Smart Connector WiFi', 'Bridge Hue','Tp-Link plug', 'Tp-Link Camera', 'Domos Door Sensor WiFi'
           ]

DEVICES_IOT_ANALYTICS = ['AmazonEcho', 'BelkinWemoMotionSensor', 'BelkinWemoSwitch', 'BlipcareBloodPressureMeter', 'DropCam', 'HpPrinter', 'InsteonCamera', 'LightBulbsLiFXSmartBulb', 'NestDropcam', 'NestProtectSmokeAlarm', 'NetatmoWeatherStation', 'NetatmoWelcome', 'PIX-STARPhoto-frame', 'SamsungGalaxyTab', 'SamsungSmartCam', 'SmartThings', 'TP-LinkDayNightCloudCamera', 'TP-LinkSmartPlug', 'TribySpeaker', 'WithingsAuraSmartSleepSensor', 'WithingsSmartBabyMonitor', 'WithingsSmartScale', 'iHome']

DEVICES_SENTINEL = ['Aria', 'D-LinkCam', 'D-LinkDayCam', 'D-LinkDoorSensor', 'D-LinkHomeHub', 'D-LinkSensor', 'D-LinkSiren', 'D-LinkSwitch', 'D-LinkWaterSensor', 'EdimaxCam1', 'EdimaxCam2', 'EdimaxPlug1101W', 'EdimaxPlug2101W', 'EdnetCam1', 'EdnetCam2', 'EdnetGateway', 'HomeMaticPlug', 'HueBridge', 'HueSwitch', 'Lightify', 'MAXGateway', 'SmarterCoffee', 'TP-LinkPlugHS100', 'TP-LinkPlugHS110', 'WeMoInsightSwitch', 'WeMoInsightSwitch2', 'WeMoLink', 'WeMoSwitch', 'WeMoSwitch2', 'Withings', 'iKettle2']

DEVICES_LABO=['Chacon Smart Connector WiFi', 'Bridge Hue', 'Tp-Link plug', 'Tp-Link Camera','Domos Door Sensor WiFi']